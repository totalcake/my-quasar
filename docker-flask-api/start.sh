#!/bin/bash

# stop all
docker-compose down

# # Install yaml pyhton package
# apt-get install -y python-yaml

# # refresh ansible inventory and ssh known_hosts
# cp rundeck/root/etc/ansible/hosts.orig rundeck/root/etc/ansible/hosts
# cp rundeck/root/root/.ssh/known_hosts.orig rundeck/root/root/.ssh/known_hosts

# # run automatic parser
# python bin/prep.py

sudo chown -R $USER data
# build images
docker-compose build

# clean unused images
docker image prune --force

# run all
docker-compose up

