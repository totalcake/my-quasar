# Flask API with postgres on docker

This is a simple demo for how to connect to a Postgres database from a python flask application.

## Running

To run this on your computer you must first install [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/)

Once you have all of that, you should be good. No need to install or even Python.

```
docker-compose up --build -d   # Run the container.

docker-compose down   # Stop and remove everything.

# Add your python code to the /app/ directory.
# At the moment, you don't have to do a fig up/down after each change.
```

SWAGGER will be available to you at `localhost:5000`.

## How it works

The `docker-compose.yml` file tells Docker that you need your Flask container and a Postgres container.
