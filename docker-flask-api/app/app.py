from flask import Flask, redirect, request, json, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask.views import MethodView
from sqlalchemy.inspection import inspect
from sqlalchemy import asc,desc
import yaml

# variables

# TODO create var file

DBUSER = 'trac'
DBPASS = 'foobarbaz'
DBHOST = 'db'
DBPORT = '5432'
DBNAME = 'trac'
URI = 'postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{db}'.format(
        user=DBUSER,
        passwd=DBPASS,
        host=DBHOST,
        port=DBPORT,
        db=DBNAME)
MODELS="models.yaml"
HOST = 'localhost'
PORT = 5000
VERSION = ""
PREFIX = "/api"+VERSION

# init Flask app with flask_sqlalchemy & CORS extensions

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# import models
models = yaml.load(open(MODELS))

# TODO : check models schema before generated model
# see https://pypi.org/project/jsonschema/

def alc2json(rows):
    data = []
    for row in rows:
        data.append(dict([(col, getattr(row,col)) for col in row.__table__.columns.keys()]))
    return data

# generating safrs & db models from yaml 
for key,value in models.items() :

    # TODO : check if a column in models.yaml is not equal to "id"
    attr = { "id" : db.Column('id', db.Integer, primary_key=True)}

    # TODO check name columns with JSONAPI specs
    # see https://jsonapi.org/format/#conventions
    for col in value.get('columns'):
        col_name = col.get('name')
        nullable = not(col.get('required',False))
        if col.get('type')== "string":
            attr[col_name] = db.Column(db.String(col.get('length')),nullable=nullable)
        if col.get('type') == "integer":
            attr[col_name] = db.Column(db.Integer,nullable=nullable)
        if col.get('type') == "foreignKey":
            attr[col_name] = db.Column(db.Integer,db.ForeignKey(col.get('key')),nullable=nullable)
 
    globals()[key] = type(key, (db.Model,),attr)

for key,value in models.items() :
    for col in value.get('columns'):
        col_name = col.get('name')
        if col.get('type') == "relationship":
            setattr(globals()[key],col_name,db.relationship(col.get('relationship'), backref = db.backref(col.get('backref'), lazy='joined')))         


    # expose api from created models
    # TODO add an auth preprocessor from keycloak

# expose api to get all models json
@app.route(PREFIX + '/_schema', methods=['GET'])
def get_models():
    return jsonify(models)

@app.errorhandler(404)
def page_not_found(e):
    return redirect("/rooms")

def rowToDict(row,relationships):
    res = dict([(col, getattr(row,col)) for col in row.__table__.columns.keys()])
    for relationship in relationships:        
        if relationship.direction.name == 'MANYTOONE':            
            field = str(relationship).split('.')[1]
            app.logger.info(field)
            if getattr(row,field):
                res[field] = getattr(row,field).name
            else :
                res[field] = None
    return res

class ObjectAPI(MethodView):

    def get(self, object_id):

        mapper = inspect(self.model)
        relationships = mapper.relationships          

        page =  int(request.args.get('page',1))
        per_page = int(request.args.get('per_page',0))
        sortBy = str(request.args.get('sortby','id'))
        descending = True if str(request.args.get('descending',"false")) == 'true' else False
        filters = request.args.get('filters',{})

        query = self.model.query

        # filter

        if filters:
            filters = json.loads(filters)
        else : 
            filters = {}

        app.logger.info(filters)  
        for attr,value in filters.items():
            query = query.filter( getattr(self.model,attr).like('%'+value+'%'))       

        if object_id is None:

            data = []
            
            if descending :
                query = query.order_by(desc(getattr(self.model,sortBy)))
            else : 
                query = query.order_by(asc(getattr(self.model,sortBy)))

            if per_page == 0:
                per_page = None
            query = query.paginate(page=page,per_page=per_page)
            for row in query.items:
                data.append(rowToDict(row, relationships))
            return jsonify({'data':data, 'count': query.total })
        else:
            query = self.model.query.filter_by(id=object_id).first()
            data = rowToDict(query,relationships)
            return jsonify({'data':data})

    def post(self):
        # create a new user
        pass

    def delete(self, object_id):
        # delete a single user
        pass

    def put(self, object_id):
        # update a single user
        pass

for key,value in models.items() :

    attr = {'model' : globals()[key] }
    globals()[key+"API"] = type(key+"API", (ObjectAPI,),attr)

    object_view = globals()[key+"API"].as_view(key+'_api')
    app.add_url_rule('/'+key+'/', defaults={'object_id': None},
                    view_func=object_view, methods=['GET',])
    app.add_url_rule('/'+key+'/', view_func=object_view, methods=['POST',])
    app.add_url_rule('/'+key+'/<int:object_id>', view_func=object_view,
                    methods=['GET', 'PUT', 'DELETE'])

# TODO review main to include code 
# TODO how create models.py, routes.py ...
# see https://hackersandslackers.com/manage-database-models-with-flask-sqlalchemy/
if __name__ == '__main__':
    db.create_all() 
    # TODO see how init and make migrations models
    # see https://flask-migrate.readthedocs.io/en/latest/
    app.run(debug=True, host='0.0.0.0',port=PORT)