import Vue from "vue";
import Router from "vue-router";
import DefaultLayout from "./layouts/Default.vue";
import Home from "./views/Home.vue";
import Positions from "./views/Positions.vue";
import Zones from "./views/Zones.vue";
import Rooms from "./views/Rooms.vue";
import Categories from "./views/Categories.vue";

import UnderConstruction from "./views/UnderConstruction.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      component: DefaultLayout,
      children: [
        {
          path: "",
          name: "home",
          component: Home
        },
        {
          path: "/positions",
          name: "positions",
          component: Positions
        },
        {
          path: "/categories",
          name: "categories",
          component: Categories
        },
        {
          path: "/zones",
          name: "zones",
          component: Zones
        },
        {
          path: "/rooms",
          name: "rooms",
          component: Rooms
        },
        {
          path: "/networks",
          name: "networks",
          component: UnderConstruction
        },
        {
          path: "/phones",
          name: "phones",
          component: UnderConstruction
        },
        {
          path: "/customers",
          name: "customers",
          component: UnderConstruction
        },
        {
          path: "/scenarios",
          name: "scenarios",
          component: UnderConstruction
        },
        {
          path: "/images",
          name: "images",
          component: UnderConstruction
        },
        {
          path: "/deployments",
          name: "deployments",
          component: UnderConstruction
        },
        {
          path: "/settings",
          name: "settings",
          component: UnderConstruction
        }
      ]
    }
  ]
});
