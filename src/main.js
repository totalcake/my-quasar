import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import Chartkick from "vue-chartkick";
import Highcharts from "highcharts";

Vue.use(Chartkick.use(Highcharts));

import axios from "axios";

Vue.prototype.$api = axios.create({
  baseURL: "http://localhost:5000"
});

import "./styles/quasar.styl";
import "quasar/dist/quasar.ie.polyfills";
import "@quasar/extras/material-icons/material-icons.css";
import {
  Quasar,
  QLayout,
  QHeader,
  QFooter,
  QDrawer,
  QPageContainer,
  QPage,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QTable,
  QTh,
  QTr,
  QTd,
  QScrollArea,
  QSeparator,
  QSelect,
  QExpansionItem,
  ClosePopup,
  Notify,
  QMenu,
  QDialog,
  QAvatar,
  QCard,
  QCardSection,
  QCardActions,
  QForm,
  QInput,
  QTooltip,
  QSpinnerHourglass
} from "quasar";

Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QHeader,
    QFooter,
    QDrawer,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QTable,
    QTh,
    QTr,
    QTd,
    QScrollArea,
    QSeparator,
    QSelect,
    QExpansionItem,
    QMenu,
    QDialog,
    QAvatar,
    QCard,
    QCardSection,
    QCardActions,
    QForm,
    QInput,
    QTooltip,
    QSpinnerHourglass
  },
  directives: {
    ClosePopup
  },
  plugins: {
    Notify
  }
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
