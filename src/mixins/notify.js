export const NotifyNotInstalled = {
  methods: {
    notifyNotInstalled() {
      this.$q.notify({
        message: "<strong>Sorry</strong> This feature is not installed yet!",
        color: "orange",
        icon: "feedback",
        position: "bottom-left",
        html: true
      });
    }
  }
};
